import api from "@forge/api";
export const run = async ({ issue }) => {
  const { key: issueKey } = issue;
  const res = await api
  const response = await api.asApp().requestJira(`/rest/api/3/issue/${issueKey}`);
  const issueJson = await response.json();
  
  return {
	//parent status category IN PROGRESS = 4)  
	result: issueJson.fields.parent.fields.status.statusCategory.id===4 ? true : false,
    errorMessage: `Issue ${issue.key} is not ready for transition because the parent status is not a IN PROGRESS status`,
  };
};